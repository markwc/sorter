#include <Sorter.h>
#include <gtest/gtest.h>

#include <array>
#include <random>

namespace
{

class Sorter_test : public ::testing::Test
{
public:
  Katas::Sorter m_sorter;

  Sorter_test()
  {
  }

  ~Sorter_test()
  {
  }

protected:
  virtual void SetUp()
  {
  }

  virtual void TearDown()
  {
  }
};

TEST_F(Sorter_test, quick_sort_int)
{
  std::array<int, 10> items;
  for (int i = 0; i < 10; i++)
  {
    items[i] = static_cast<int>(9 - i);
  }
  m_sorter.quick(items.data(), 0, 9);
  for (int i = 0; i < 10; i++)
  {
    EXPECT_EQ(items[i], i);
  }
}
} // namespace