#include <Sorter.h>

#include <array>
#include <iostream>

int main()
{
  Katas::Sorter sorter;
  std::array<int, 10> items;
  for (int i = 0; i < 10; i++)
  {
    items[i] = static_cast<int>(9 - i);
  }
  sorter.quick(items.data(), 0, 9);
  for (int i = 0; i < 10; i++)
  {
    std::cout << items[i] << std::endl;
  }
  return 0;
}