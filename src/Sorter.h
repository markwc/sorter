/// @file Sorter.h

#ifndef SRC_SORTER_H
#define SRC_SORTER_H

namespace Katas
{

class Sorter
{
public:
  Sorter(void);

  ~Sorter(void);

  void quick(int *items, int low, int high);

private:
  struct Impl;
  Impl *m_p_impl;

  Sorter &operator=(const Sorter &);
  Sorter(const Sorter &);
};
} // namespace Katas

#endif // #define SRC_SORTER_H
