/// @file Sorter.cpp

#include <Sorter.h>

namespace Katas
{
// PRIVATE IMPLEMENTATION
struct Sorter::Impl
{
  Impl(void)
  {
  }

  ~Impl(void)
  {
  }

  template <class T> int partition(T *items, int low, int high)
  {
    T pivot = items[high];
    int i = low - 1;
    for (int j = low; j <= high; j++)
    {
      if (items[j] < pivot)
      {
        i++;
        swap(items[i], items[j]);
      }
    }
    swap(items[i + 1], items[high]);
    return (i + 1);
  }

  template <class T> void swap(T &item_low, T &item_high)
  {
    T temp = item_low;
    item_low = item_high;
    item_high = temp;
  }
};

// PUBLIC IMPLEMENTATION

Sorter::Sorter(void) : m_p_impl(new Impl)
{
}

Sorter::~Sorter(void)
{
  delete m_p_impl;
}

void Sorter::quick(int *items, int low, int high)
{
  if (low < high)
  {
    int partition_index = m_p_impl->partition(items, low, high);
    quick(items, low, partition_index - 1);
    quick(items, partition_index + 1, high);
  }
}

} // namespace Katas
